fetch("https://restcountries.com/v3.1/all")
  .then((obj) => obj.json())
  .then((countryData) => {
    loaderBlocker();
    countryDriver(countryData);
  })
  .catch((err) => {
    errorFunction();
  });

function countryDriver(countryData) {
  let countryNameData = 0;
  let body = document.querySelector("body");

  let header = document.createElement("header");
  header.setAttribute("id", "header-content");
  body.append(header);

  let worldData = document.createElement("h1");
  worldData.innerHTML = "Where in the world?";
  header.append(worldData);

  let searchCountry = document.createElement("input");
  searchCountry.setAttribute("id", "filter");
  searchCountry.setAttribute("type", "text");
  searchCountry.setAttribute("placeholder", "Search for a Country ...");
  header.append(searchCountry);

  let regionFilter = document.createElement("select");
  regionFilter.setAttribute("id", "country-arrange-options");
  header.append(regionFilter);

  let allCountries = document.createElement("div");
  allCountries.setAttribute("id", "country");
  body.append(allCountries);

  let regionSelected = document.createElement("option");
  regionSelected.setAttribute("disabled", "");
  regionSelected.setAttribute("selected", "");
  regionSelected.innerHTML = "Filter by Region ...";
  regionFilter.append(regionSelected);

  let regionAfrica = document.createElement("option");
  regionAfrica.setAttribute("class", "dropdown-option");
  regionAfrica.innerHTML = "Africa";
  regionFilter.append(regionAfrica);

  let regionAmerica = document.createElement("option");
  regionAmerica.setAttribute("class", "dropdown-option");
  regionAmerica.innerHTML = "Americas";
  regionFilter.append(regionAmerica);

  let regionAsia = document.createElement("option");
  regionAsia.setAttribute("class", "dropdown-option");
  regionAsia.innerHTML = "Asia";
  regionFilter.append(regionAsia);

  let regionOciania = document.createElement("option");
  regionOciania.setAttribute("class", "dropdown-option");
  regionOciania.innerHTML = "Oceania";
  regionFilter.append(regionOciania);

  let regionEurope = document.createElement("option");
  regionEurope.setAttribute("class", "dropdown-option");
  regionEurope.innerHTML = "Europe";
  regionFilter.append(regionEurope);

  countryData.forEach((country) => {
    let countryFlag = country.flags.png;
    let countryName = country.name.official;
    let countryPopulation = country.population;
    let countryRegion = country.region;
    let countryCapital = "Not Applicable";
    if (country.hasOwnProperty("capital")) {
      countryCapital = country.capital[0];
    }

    let div = document.createElement("div");
    div.setAttribute("class", "country-detail");

    let image = document.createElement("img");
    image.setAttribute("src", countryFlag);
    image.setAttribute("class", "country-flag");

    let name = document.createElement("h2");
    name.setAttribute("class", "country-name");
    name.innerHTML = countryName;

    let population = document.createElement("p");
    population.innerHTML = `<b>Population:</b> ${countryPopulation}`;

    let region = document.createElement("p");
    region.innerHTML = `<b>Region:</b> ${countryRegion}`;

    let capital = document.createElement("p");
    capital.innerHTML = `<b>Capital:</b> ${countryCapital}`;

    allCountries.append(div);
    div.append(image);
    div.append(name);
    div.append(population);
    div.append(region);
    div.append(capital);

    let filterBySearch = document.getElementById("filter");
    filterBySearch.addEventListener("keyup", filterItemsBySearch);

    let filterByDropDown = document.getElementById("country-arrange-options");
    filterByDropDown.addEventListener("change", filterItemsByDropDown);

     });

     document.querySelectorAll('.country-detail').forEach(item => {
      item.addEventListener('click', function countryDetailsEvent(event) {
        if(!(document.querySelector("#country-data-div"))){
        if(event.target.innerHTML.substring(1,4) === "img"){
          countryNameData = (event.target.firstChild.nextSibling.innerHTML);
          popUp(countryNameData,countryData);
        } else {
          countryNameData = (event.target.parentNode.firstChild.nextSibling.innerHTML);
          popUp(countryNameData,countryData);
        }
      } else {
        document.querySelector("#country-data-div").remove();
        document.getElementById("country").style.opacity = "1";
      }
      });
    });
}

function filterItemsBySearch(e) {
  let text = e.target.value.toLowerCase();
  let items = document.getElementsByClassName("country-detail");
  Array.from(items).forEach(function (item) {
    let itemName = item.firstChild.nextSibling.textContent;
    if (itemName.toLowerCase().includes(text)) {
      item.style.display = "block";
    } else {
      item.style.display = "none";
    }
  });
}

function filterItemsByDropDown(e) {
  let text = e.target.value.toLowerCase();
  let items = document.getElementsByClassName("country-detail");
  Array.from(items).forEach(function (item) {
    let itemName = item.lastChild.previousSibling.textContent.substring(8);
    if (itemName.toLowerCase() === text) {
      item.style.display = "block";
    } else {
      item.style.display = "none";
    }
  });
}

function loaderBlocker() {
  const loader = document.getElementById("center");
  loader.remove();
}

function errorFunction() {
  let body = document.querySelector("body");
  body.style.backgroundImage =
    "url('https://thumbs.dreamstime.com/z/oops-error-website-template-human-hand-shows-hole-message-page-not-found-vector-illustration-web-ui-design-212068062.jpg')";
  body.style.backgroundRepeat = "no-repeat";
  body.style.backgroundSize = "cover";
}

function popUp(countryName,countryData) {
  countryData.forEach(country => {
    if(country.name.official === countryName) {

      let body = document.querySelector("body");

      let items = document.getElementById("country");
      items.style.opacity = "0.1";
      
      let countryDataDiv = document.createElement("div");
      countryDataDiv.setAttribute("id","country-data-div");
      body.append(countryDataDiv);

      let countryFlag = country.flags.png;
      let image = document.createElement("img");
      image.setAttribute("src", countryFlag);
      image.setAttribute("class", "countryData-flag");
      countryDataDiv.append(image);

      let countryOtherDetails = document.createElement("div");
      countryOtherDetails.setAttribute("id","country-other-details");
      countryDataDiv.append(countryOtherDetails);

      let countryName = country.name.official;
      let name = document.createElement("h2");
      name.setAttribute("class", "countryD-name");
      name.innerHTML = countryName;
      countryOtherDetails.append(name);

      let countryPopulation = country.population; 
      let population = document.createElement("p");
      population.innerHTML = `<b>Population: </b> ${countryPopulation}`;
      countryOtherDetails.append(population);

      let countryRegion = country.region; 
      let region = document.createElement("p");
      region.innerHTML = `<b>Region: </b> ${countryRegion}`;
      countryOtherDetails.append(region);

      let countrySubRegion = country.subregion; 
      let subRegion = document.createElement("p");
      subRegion.innerHTML = `<b>Sub Region: </b> ${countrySubRegion}`;
      countryOtherDetails.append(subRegion);

      let countryCapital = "Not Applicable";
      if (country.hasOwnProperty("capital")) {
          countryCapital = country.capital[0];
      }
      let capital = document.createElement("p");
      capital.innerHTML = `<b>Capital: </b> ${countryCapital}`;
      countryOtherDetails.append(capital);

      let coutryTopLevelDomain = country.tld[0];
      let topDomain = document.createElement("p");
      topDomain.innerHTML = `<b>Top Level Domain: </b> ${coutryTopLevelDomain}`;
      countryOtherDetails.append(topDomain);

      let countryCurrency = Object.values(country.currencies);
      let currency = countryCurrency.map((currency)=>{
        return currency.name;
      }).join(", ");
      let currencyData = document.createElement("p");
      currencyData.innerHTML = `<b>Currencies: </b> ${currency}`;
      countryOtherDetails.append(currencyData);

      let countryLanguages = Object.values(country.languages);
      let language = countryLanguages.map((language)=>{
        return language;
      }).join(", ");
      let languageData = document.createElement("p");
      languageData.innerHTML = `<b>Languages: </b> ${language}`;
      countryOtherDetails.append(languageData);

      if(country.borders !== undefined){
      let countryBorders = Object.values(country.borders);
      let borders = countryBorders.map((country)=>{
        return country;
      }).join(", ");
      let borderData = document.createElement("p");
      languageData.innerHTML = "<b>Borders: </b>" + borders;
      countryOtherDetails.append(borderData);
    }

      let cancelButton = document.createElement("button");
      cancelButton.setAttribute("id","cancel-button");
      cancelButton.onclick = backToCountries;
      cancelButton.innerHTML = "<b>Back to Countries</b>";
      countryOtherDetails.append(cancelButton);

      document.onkeyup = (event) => {
        event = event || window.event;
        let isEscape = false;
        if("key" in event) {
          isEscape = (event.key === "Escape" || event.key === "Esc");
        } else {
          isEscape = (event.keyCode === 27);
        }
        if(isEscape) {
          backToCountries();
        }
      };
  }
});
}

function backToCountries() {
    document.querySelector("#country-data-div").remove();
    let country = document.querySelector("#country");
    country.style.display = "grid";
    country.style.opacity = "1";
}

